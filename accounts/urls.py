from django.urls import path
from .views import user_login, log_out, user_signup

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", log_out, name="logout"),
    path("signup/", user_signup, name="signup"),
]
